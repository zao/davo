#include <http_parser.h>
#include <polarssl/ssl.h>
#include <uv.h>

#include <iostream>
#include <list>
#include <mutex>
#include <thread>

struct listener_t {
    listener_t() {}
    uv_tcp_t sock;
};

enum { CLIENT_BUFSIZE = 8192 };

struct transfer_t {
    int64_t byte_count;
    int64_t bytes_moved;
};

enum { CLIENT_MAX_HEADERS = 1024 };

struct header_t {
    char const* key;
    char const* value;
};

struct client_t {
    client_t() : ssl_ready(false) {}
    uv_tcp_t sock;
    uv_write_t tx;
    bool ssl_ready;
    char rxbuf[CLIENT_BUFSIZE];
    char txbuf[CLIENT_BUFSIZE];
    http_parser_settings parser_settings;
    http_parser parser;
};

std::mutex state_mutex;
std::list<listener_t*> listeners;
std::list<client_t*> clients;

static void client_rxbuf_alloc(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf) {
    auto client = static_cast<client_t*>(handle->data);
    buf->base = client->rxbuf;
    buf->len = CLIENT_BUFSIZE;
}

static void client_on_close(uv_handle_t* handle) {
    auto client = static_cast<client_t*>(handle->data);
    delete client;
}

static void client_on_read(uv_stream_t* stream, ssize_t nread, uv_buf_t const* buf) {
    auto client = static_cast<client_t*>(stream->data);
    if (nread > 0) {
        http_parser_init(&client->parser, HTTP_REQUEST);
        int nparsed = http_parser_execute(&client->parser, &client->parser_settings, buf->base, nread/2);
        nparsed += http_parser_execute(&client->parser, &client->parser_settings, buf->base + nread/2, nread-nread/2);
        char const* filename =
#if defined(WIN32)
                "D:\\Temp\\log.txt";
#else
                "/tmp/log.txt";
#endif
        FILE* f = fopen(filename, "a");
        fprintf(f, "moo, %llu/%llu parsed.\n", static_cast <unsigned long long>(nparsed), static_cast <unsigned long long>(nread));
        auto& p = client->parser;
        fprintf(f,
                "  nread: %lu\n"
                "  content_length: %llu\n"
                "  http_major: %u\n"
                "  http_minor: %u\n"
                "  method: %u\n"
                "  http_errno: %u\n",
                static_cast <unsigned long>(p.nread), static_cast <unsigned long long>(p.content_length),
                p.http_major, p.http_minor, p.method, p.http_errno);
        fclose(f);
    }
    if (nread < 0) {
        uv_read_stop(stream);
        uv_close(reinterpret_cast<uv_handle_t*>(stream), client_on_close);
    }
}

static void listen_on_connection(uv_stream_t* server, int status) {
    auto client = new client_t();
    uv_tcp_init(server->loop, &client->sock);
    client->sock.data = client;
    uv_accept(server, reinterpret_cast<uv_stream_t*>(&client->sock));
    std::lock_guard<std::mutex> lk(state_mutex);
    clients.push_back(client);
    uv_read_start(reinterpret_cast<uv_stream_t*>(&client->sock), &client_rxbuf_alloc, &client_on_read);
}

int main() {
    uv_loop_t loop;
    uv_loop_init(&loop);

    {
        sockaddr_in listen_saddr;
        uv_ip4_addr("127.0.0.1", 9080, &listen_saddr);
        auto* listener = new listener_t();
        uv_tcp_init(&loop, &listener->sock);
        uv_tcp_bind(&listener->sock, reinterpret_cast<sockaddr const*>(&listen_saddr), 0);
        uv_listen(reinterpret_cast<uv_stream_t*>(&listener->sock), 5, listen_on_connection);
        std::lock_guard<std::mutex> lk(state_mutex);
        listeners.push_back(listener);
    }

    // ssl_context ssl;
    // ssl_init(&ssl);
    uv_run(&loop, UV_RUN_DEFAULT);
    // ssl_close_notify(&ssl);
    uv_loop_close(&loop);
    return 0;
}
